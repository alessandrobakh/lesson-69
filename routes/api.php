<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('articles', [\App\Http\Controllers\ArticlesController::class, 'index'])->name('articles.index');
Route::get('articles/{article}/comments', [\App\Http\Controllers\CommentsController::class, 'index'])->name('articles.comments.index');

Route::apiResource('articles', App\Http\Controllers\ArticlesController::class)->except(['index'])->middleware('auth:api');
Route::apiResource('articles.comments', \App\Http\Controllers\CommentsController::class)->except(['index'])->middleware('auth:api');
