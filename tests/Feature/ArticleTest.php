<?php
namespace Tests\Feature;
use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Laravel\Passport\Passport;

class ArticleTest extends TestCase
{
    use WithFaker;

    /**
     * @var = App\Models\User
     */
    private $user;

    /**
     * @var = App\Models\Article
     */
    private $articles;

    /**
     * @var = App\Models\Article
     */
    private $firstArticle;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->articles = Article::factory()->count(5)->for($this->user)->create();
        $this->firstArticle = $this->articles->first();
    }

    /**
     * @group articles
     * @return void
     */
    public function test_can_create_article()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'user_id' => $this->user->id
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $resp = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('articles', ['id' => $resp->data->id]);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_validation_error_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
        $this->assertArrayHasKey('content', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_title_validation_error_required_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_title_validation_error_min3_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => 'q',
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_title_validation_error_string_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => 12123,
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_articles_index()
    {
        $response = $this->getJson(route('articles.index'));
        $response->assertStatus(200);

        foreach ($this->articles as $article){
            $response->assertSeeText($article->title);
            $response->assertSeeText($article->content);
            $response->assertSeeText($article->user->name);
        }
    }

    /**
     * @group articles
     * @return void
     */
    public function test_articles_show()
    {
        Passport::actingAs(
            $this->user
        );
        $response = $this->getJson("/api/articles/{$this->firstArticle->id}");
        $response->assertStatus(200);

        $response->assertSeeText($this->firstArticle->title);
        $response->assertSeeText($this->firstArticle->content);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_articles_show_error_without_auth()
    {
        $response = $this->getJson("/api/articles/{$this->firstArticle->id}");
        $response->assertStatus(401);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_can_update_article()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'user_id' => $this->firstArticle->user->id
        ];
        $request = $this->putJson(route('articles.update', ['article' => $this->firstArticle]), $data);
        $resp = json_decode($request->getContent());
        $request->assertSuccessful();
        $this->assertDatabaseHas('articles', ['id' => $resp->data->id]);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_validation_error_update()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [];
        $request = $this->putJson(route('articles.update', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
        $this->assertArrayHasKey('content', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_content_validation_error_required_update()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => $this->faker->sentence,
            'user_id' => $this->user->id,
        ];
        $request = $this->putJson(route('articles.update', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('content', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_content_validation_error_min5_update()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => $this->faker->sentence,
            'content' => 'q',
            'user_id' => $this->user->id,
        ];
        $request = $this->putJson(route('articles.update', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('content', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_content_validation_error_string_update()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'title' => $this->faker->sentence,
            'content' => true,
            'user_id' => $this->user->id,
        ];
        $request = $this->putJson(route('articles.update', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('content', (array)$payload->errors);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_destroy_article()
    {
        Passport::actingAs(
            $this->user
        );
        $request = $this->deleteJson(route('articles.destroy', ['article' => $this->firstArticle]));
        $request->assertStatus(204);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_destroy_article_error_without_auth()
    {
        $request = $this->deleteJson(route('articles.destroy', ['article' => $this->firstArticle]));
        $request->assertStatus(401);
    }
}
