<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Laravel\Passport\Passport;

class CommentTest extends TestCase
{
    use WithFaker;

    /**
     * @var = App\Models\User
     */
    private $user;

    /**
     * @var = App\Models\Article
     */
    private $articles;

    /**
     * @var = App\Models\Article
     */
    private $firstArticle;

    /**
     * @var = App\Models\Comment
     */
    private $comments;

    /**
     * @var = App\Models\Comment
     */
    private $firstComment;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->articles = Article::factory()->count(5)->for($this->user)->create();
        $this->firstArticle = $this->articles->first();
        $this->comments = Comment::factory()->count(10)->for($this->user)->for($this->firstArticle)->create();
        $this->firstComment = $this->comments->first();
    }

    /**
     * @group comments
     * @return void
     */
    public function test_can_show_comment()
    {
        Passport::actingAs(
            $this->user
        );
        $response = $this->getJson(route('articles.comments.show', ['article' => $this->firstArticle, 'comment' => $this->firstComment]));
        $response->assertStatus(200);
        $response->assertSeeText($this->firstComment->body);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_cannot_show_comment_without_auth()
    {
        $response = $this->getJson(route('articles.comments.show', ['article' => $this->firstArticle, 'comment' => $this->firstComment]));
        $response->assertStatus(401);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_comments_index()
    {
        $response = $this->getJson(route('articles.comments.index', ['article' => $this->firstArticle]));
        $response->assertStatus(200);

        foreach ($this->comments as $comment){
            $response->assertSeeText($comment->body);
        }
    }

    /**
     * @group comments
     * @return void
     */
    public function test_destroy_comment()
    {
        Passport::actingAs(
            $this->user
        );
        $request = $this->deleteJson(route('articles.comments.destroy', ['article' => $this->firstArticle, 'comment' => $this->firstComment]));
        $request->assertStatus(204);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_destroy_comment_error_without_auth()
    {
        $request = $this->deleteJson(route('articles.comments.destroy', ['article' => $this->firstArticle, 'comment' => $this->firstComment]));
        $request->assertStatus(401);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_can_create_comment()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'body' => $this->faker->paragraph(),
            'article_id' => $this->firstArticle->id,
            'user_id' => $this->user->id
        ];
        $request = $this->postJson(route('articles.comments.store', ['article' => $this->firstArticle]), $data);
        $resp = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('comments', ['id' => $resp->data->id]);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_validation_error_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [];
        $request = $this->postJson(route('articles.comments.store', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
        $this->assertArrayHasKey('article_id', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_body_validation_error_required_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'article_id' => $this->firstArticle->id,
            'user_id' => $this->user->id
        ];
        $request = $this->postJson(route('articles.comments.store', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_body_validation_error_max1000_store()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'body' => $this->faker->words(1500),
            'article_id' => $this->firstArticle->id,
            'user_id' => $this->user->id
        ];
        $request = $this->postJson(route('articles.comments.store', ['article' => $this->firstArticle]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_can_update_comment()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'body' => $this->faker->paragraph(),
            'article_id' => $this->firstArticle->id,
            'user_id' => $this->user->id
        ];
        $request = $this->putJson(route('articles.comments.update', ['article' => $this->firstArticle, 'comment' => $this->firstComment]), $data);
        $resp = json_decode($request->getContent());
        $request->assertSuccessful();
        $this->assertDatabaseHas('comments', ['id' => $resp->data->id]);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_validation_error_update()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [];
        $request = $this->putJson(route('articles.comments.update', ['article' => $this->firstArticle, 'comment' => $this->firstComment]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
        $this->assertArrayHasKey('article_id', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }

    /**
     * @group comments
     * @return void
     */
    public function test_body_validation_error_required_update()
    {
        Passport::actingAs(
            $this->user
        );
        $data = [
            'article_id' => $this->firstArticle->id,
            'user_id' => $this->user->id,
        ];
        $request = $this->putJson(route('articles.comments.update', ['article' => $this->firstArticle, 'comment' => $this->firstComment]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }

}
